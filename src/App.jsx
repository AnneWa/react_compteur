import { useState } from 'react'
import './App.css'

function App() {
  const [count, setCount] = useState(0)

  
  


  return (
    <div className="App">
      
      <h2>Compteur de participants</h2>
      <div className='counter'>{count}</div>

      <div className="card">
        <button onClick={() => setCount((count) => count + 1)}> + </button>

        <button onClick={() => setCount((count) => count > 0 ? count - 1 : 0)}> - </button>
      </div>

        <button onClick={() => setCount(0)}>Remise à zéro</button>
      
    </div>
  )
}

export default App
